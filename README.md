# redis-demo

Sebagai pembelajaran redis

download depedencies
```bash
composer install
```
kemudian download redis for windows [link](https://github.com/dmajkic/redis/downloads), lalu jalankan redis-server.exe

hasil redis
![](https://gitlab.com/maulana20/redis-demo/-/raw/master/image/redis.PNG)

jalankan test redis
```bash
php run.php
```

![](https://gitlab.com/maulana20/redis-demo/-/raw/master/image/run.PNG)

jika kita test pada redis-cli.exe, maka hasilnya
![](https://gitlab.com/maulana20/redis-demo/-/raw/master/image/redis-cli.PNG)
