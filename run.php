<?php
require_once 'vendor/autoload.php';

use Predis\Client;

try {
	$redis = new Client();
	
	$redis->set('nama', 'maulana saputra');
	$redis->set('nim', '11115555');
	$redis->set('kelas', '11.1C.01');
	
	echo $redis->get('nama') . "\n";
	echo $redis->get('nim') . "\n";
	echo $redis->get('kelas') . "\n";
}
catch (Exception $e) {
    die ($e->getMessage());
}
